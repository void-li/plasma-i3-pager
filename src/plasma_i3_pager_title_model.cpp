/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_pager_title_model.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

// ----------

namespace lvd::plasma::i3::pager {

}  // namespace lvd::plasma::i3::pager
