/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_pager_window.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QJsonValue>

// ----------

namespace lvd::plasma::i3::pager {

PlasmaI3PagerWindow PlasmaI3PagerWindow::create(
    const QJsonObject& qjsonobject) {
  PlasmaI3PagerWindow plasma_i3_pager_window;

  auto name_v = qjsonobject["name"];
  if (name_v.isString()) {
    plasma_i3_pager_window.name_ = static_cast<QString>(name_v.toString());
  }

  auto iden_v = qjsonobject[ "id" ];
  if (iden_v.isDouble()) {
    plasma_i3_pager_window.iden_ = static_cast<quint64>(iden_v.toDouble());
  }

  auto output_v = qjsonobject["output" ];
  if (output_v.isString()) {
    plasma_i3_pager_window.output_  = output_v .toString();
  }

  return plasma_i3_pager_window;
}

}  // namespace lvd::plasma::i3::pager
