/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_pager_pager_model.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <optional>

#include <i3ipc.h>

#include "lvd/shield.hpp"

#include "plasma_i3_pager_wspace.hpp"

// ----------

namespace lvd::plasma::i3::pager {

QHash<int, QByteArray> PlasmaI3PagerPagerModel::roleNames() const {
  return
  LVD_SHIELD;

  QHash<int, QByteArray> roleNames;

  for (auto role  = Roles::__vals_begin__();
            role != Roles::__vals_end__  ();
            role ++) {
    roleNames[*role] = Roles::To_String(*role);
  }

  return roleNames;

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return QHash<int, QByteArray>();
  });
}

int PlasmaI3PagerPagerModel::columnCount(const QModelIndex& index) const {
  return
  LVD_SHIELD;

  return index.isValid() ? 0 : parent_->wspaces_.size(), 1;

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return 0;
  });
}

int PlasmaI3PagerPagerModel::   rowCount(const QModelIndex& index) const {
  return
  LVD_SHIELD;

  return index.isValid() ? 0 : parent_->wspaces_.size();

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return 0;
  });
}

QVariant PlasmaI3PagerPagerModel::  data(const QModelIndex& index,
                                         int                role ) const {
  return
  LVD_SHIELD;

  if (   index.row() >=                  0x00
      && index.row() < parent_->wspaces_.size()) {
    const PlasmaI3PagerWspace& wspace = parent_->wspaces_[index.row()];

    switch (role) {
      case Roles::DisplayName:
        return QVariant(wspace.display_name());

      case Roles::SortingName:
        return QVariant(wspace.sorting_name());

      case Roles::Focused:
        return QVariant(wspace.focused().value_or(false));

      case Roles::Visible:
        return QVariant(wspace.visible().value_or(false));

      case Roles::Urgent :
        return QVariant(wspace.urgent ().value_or(false));
    }
  }

  LVD_LOG_W() << "invalid role";
  return QVariant();

  LVD_SHIELD_FUN([] (const QString& message) {
    LVD_LOG_W() << message;
    return QVariant();
  });
}

// ----------

void PlasmaI3PagerPagerModel::select(int index) {
  LVD_LOG_T() << index;
  LVD_SHIELD;

  if (   index >= 0
      && index <= parent_->wspaces_.size() - 1) {
    const PlasmaI3PagerWspace& wspace = parent_->wspaces_[index];

    parent_->i3ipc_->sendMessage(i3ipc::IpcType::Command,
                                 "workspace " + wspace.name().toLocal8Bit());
  }

  LVD_SHIELD_END;
}

void PlasmaI3PagerPagerModel::select_l() {
  LVD_LOG_T();
  LVD_SHIELD;

  parent_->i3ipc_->sendMessage(i3ipc::IpcType::Command,
                               "workspace prev_on_output");

  LVD_SHIELD_END;
}

void PlasmaI3PagerPagerModel::select_r() {
  LVD_LOG_T();
  LVD_SHIELD;

  parent_->i3ipc_->sendMessage(i3ipc::IpcType::Command,
                               "workspace next_on_output");

  LVD_SHIELD_END;
}

}  // namespace lvd::plasma::i3::pager
