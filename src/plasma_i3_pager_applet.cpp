/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_pager_applet.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QQmlEngine>

#include <KPluginMetaData>  // IWYU pragma: keep
// IWYU pragma: no_include <kpluginmetadata.h>

// ----------

namespace lvd::plasma::i3::pager {

PlasmaI3PagerApplet::PlasmaI3PagerApplet(QObject* parent, const QVariantList& args)
    : Plasma::Applet(parent, KPluginMetaData(), args) {
  lvd::Logger::install();

  // ----------

  plasma_i3_pager_ = new PlasmaI3Pager(this);
  qmlRegisterInterface<PlasmaI3Pager>("PlasmaI3Pager", 1);

  QMetaObject::invokeMethod(plasma_i3_pager_, [&] {
    plasma_i3_pager_->execute();
  }, Qt::QueuedConnection);
}

}  // namespace lvd::plasma::i3::pager

// ----------

K_EXPORT_PLASMA_APPLET_WITH_JSON(plasma_i3_pager_applet,
  lvd::plasma::i3::pager::PlasmaI3PagerApplet, "metadata.json")

#include "plasma_i3_pager_applet.moc"  // IWYU pragma: keep
