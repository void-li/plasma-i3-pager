/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13

// ----------

Item {
  id: base

  implicitHeight: 10 + text.implicitHeight
  implicitWidth:  10 + text.implicitWidth

  property color background_color
  property color foreground_color
  property color borderline_color

  property string text

  Rectangle {
    anchors.fill: parent

    border.color: base.borderline_color
    border.width: 1

    color: base.background_color

    Behavior on color {
      ColorAnimation {
        duration: 256
      }
    }

    Text {
      id: text

      anchors.centerIn: parent

      color: base.foreground_color

      text: base.text
    }
  }
}
