/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13

// ----------

Item {
  id: base

  implicitHeight: grid.height
  implicitWidth:  grid.width

  property bool horizontal_layout

  property color default_background_color
  property color default_foreground_color
  property color default_borderline_color

  property color visible_background_color
  property color visible_foreground_color
  property color visible_borderline_color

  property color focused_background_color
  property color focused_foreground_color
  property color focused_borderline_color

  property color  urgent_background_color
  property color  urgent_foreground_color
  property color  urgent_borderline_color

  property color control_background_color
  property color control_foreground_color
  property color control_borderline_color

  property var pager_model
  property var modes_model

  Grid {
    id: grid

    height: base.horizontal_layout ? parent.height : childrenRect.height
    width: !base.horizontal_layout ? parent.width  : childrenRect.width

    columns: base.horizontal_layout ? 2 : 1
    spacing: 2

    Item {
      height: base.horizontal_layout ? parent.height : list_view.height
      width: !base.horizontal_layout ? parent.width  : list_view.width

      MouseArea {
        anchors.fill: parent

        onWheel: {
          if (wheel.angleDelta.y > 0) {
            base.pager_model.select_l()
          } else {
            base.pager_model.select_r()
          }
        }
      }

      ListView {
        id: list_view

        height: base.horizontal_layout ? parent.height : childrenRect.height
        width: !base.horizontal_layout ? parent.width  : childrenRect.width

        spacing: grid.spacing

        orientation: base.horizontal_layout ? ListView.Horizontal : ListView.Vertical

        model: base.pager_model

        delegate: Item {
          height: base.horizontal_layout ? parent.height : indicator.height
          width: !base.horizontal_layout ? parent.width  : indicator.width

          MouseArea {
            anchors.fill: parent

            onClicked: {
              base.pager_model.select(model.index)
            }
          }

          Item {
            height: base.horizontal_layout ? parent.height : indicator.height + 2
            width: !base.horizontal_layout ? parent.width  : indicator.width  + 2

            Indicator {
              id: indicator
              anchors.centerIn: parent

              height: base.horizontal_layout ? (parent.height -2) : Math.max(implicitHeight,         width)
              width: !base.horizontal_layout ? (parent.width  -2) : Math.max(        height, implicitWidth)

              background_color: model.Urgent  ? base. urgent_background_color :
                                model.Focused ? base.focused_background_color :
                                model.Visible ? base.visible_background_color :
                                                base.default_background_color

              foreground_color: model.Urgent  ? base. urgent_foreground_color :
                                model.Focused ? base.focused_foreground_color :
                                model.Visible ? base.visible_foreground_color :
                                                base.default_foreground_color

              borderline_color: model.Urgent  ? base. urgent_borderline_color :
                                model.Focused ? base.focused_borderline_color :
                                model.Visible ? base.visible_borderline_color :
                                                base.default_borderline_color

              text: model.DisplayName
            }
          }
        }
      }
    }

    Item {
      height: base.horizontal_layout ? parent.height : indicator.height + 2
      width: !base.horizontal_layout ? parent.width  : indicator.width  + 2

      Indicator {
        id: indicator
        anchors.centerIn: parent

        height: base.horizontal_layout ? (parent.height -2) : Math.max(implicitHeight,         width)
        width: !base.horizontal_layout ? (parent.width  -2) : Math.max(        height, implicitWidth)

        visible: text !== "" && text !== "default"

        background_color: base.control_background_color

        foreground_color: base.control_foreground_color

        borderline_color: base.control_borderline_color

        text: base.modes_model.modes
      }
    }
  }
}
