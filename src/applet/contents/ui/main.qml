/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.13

import org.kde.plasma.core 2.0
import org.kde.plasma.plasmoid 2.0

import "main.js" as JS

// ----------

Item {
  id: main

  property bool horizontal_layout: plasmoid.formFactor === Types.Horizontal

  Plasmoid.compactRepresentation: Item {
    Layout.fillHeight: main.horizontal_layout
    Layout.fillWidth: !main.horizontal_layout

    Layout.preferredHeight: childrenRect.height
    Layout.preferredWidth:  childrenRect.width

    Root {
      height: main.horizontal_layout ? parent.height : childrenRect.height
      width: !main.horizontal_layout ? parent.width  : childrenRect.width

      horizontal_layout: main.horizontal_layout

      default_background_color: JS.amalgamate(theme.  backgroundColor, theme.      textColor, 0.9)
      default_foreground_color: JS.amalgamate(theme.  backgroundColor, theme.      textColor, 0.0)
      default_borderline_color: JS.amalgamate(theme.  backgroundColor, theme.      textColor, 0.8)

      visible_background_color: JS.amalgamate(theme.        linkColor, theme.backgroundColor, 0.5)
      visible_foreground_color: JS.amalgamate(theme.        linkColor, theme.backgroundColor, 0.0)
      visible_borderline_color: JS.amalgamate(theme.        linkColor, theme.      textColor, 0.8)

      focused_background_color: JS.amalgamate(theme.        linkColor, theme.backgroundColor, 0.9)
      focused_foreground_color: JS.amalgamate(theme.        linkColor, theme.backgroundColor, 0.0)
      focused_borderline_color: JS.amalgamate(theme.        linkColor, theme.      textColor, 0.8)

       urgent_background_color: JS.amalgamate(theme.negativeTextColor, theme.backgroundColor, 0.9)
       urgent_foreground_color: JS.amalgamate(theme.negativeTextColor, theme.backgroundColor, 0.0)
       urgent_borderline_color: JS.amalgamate(theme.negativeTextColor, theme.      textColor, 0.8)

      control_background_color: JS.amalgamate(theme.positiveTextColor, theme.backgroundColor, 0.9)
      control_foreground_color: JS.amalgamate(theme.positiveTextColor, theme.backgroundColor, 0.0)
      control_borderline_color: JS.amalgamate(theme.positiveTextColor, theme.      textColor, 0.8)

      pager_model: plasmoid.nativeInterface.nativeInterface().pager_model()
      modes_model: plasmoid.nativeInterface.nativeInterface().modes_model()

      Component.onCompleted: {
        pager_model.output = Qt.binding(function() {
          return Screen.name
        })
      }
    }
  }
}
