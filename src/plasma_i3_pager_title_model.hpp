/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "plasma_i3_pager.hpp"

// ----------

namespace lvd::plasma::i3::pager {

class PlasmaI3PagerTitleModel : public QObject {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaI3Pager)

  Q_PROPERTY(QString output READ output WRITE set_output NOTIFY output_changed)

  Q_PROPERTY(QString title READ title NOTIFY title_changed)

 public:
  PlasmaI3PagerTitleModel(PlasmaI3Pager* parent)
      : QObject(parent),
        parent_(parent) {}

 public:
  QString output() const {
    return parent_->output_;
  }
  void set_output(const QString& output) {
    parent_->update_output(output);
  }

  QString title() const {
    return parent_->window_.name();
  }

 signals:
  void output_changed(const QString& output);
  void title_changed(const QString& title);

 private:
  PlasmaI3Pager* parent_ = nullptr;
  friend PlasmaI3Pager;
};

}  // namespace lvd::plasma::i3::pager
