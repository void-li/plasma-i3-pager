/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QString>

// ----------

namespace lvd::plasma::i3::pager::config {

QString App_Name();
QString App_Vers();

QString Org_Name();
QString Org_Addr();

}  // namespace lvd::plasma::i3::pager::config
