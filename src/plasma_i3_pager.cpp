/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_pager.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <algorithm>
#include <utility>

#include <QCollator>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonValueRef>
#include <QQmlEngine>
#include <QTimer>

#include <i3ipc.h>

#include "lvd/shield.hpp"

#include "plasma_i3_pager_modes_model.hpp"
#include "plasma_i3_pager_pager_model.hpp"
#include "plasma_i3_pager_title_model.hpp"

// ----------

namespace lvd::plasma::i3::pager {

PlasmaI3Pager::PlasmaI3Pager(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  i3ipc_ = new i3ipc(this);

  connect(i3ipc_, &i3ipc::modeEvent,
          this, &PlasmaI3Pager::on_i3mode_event);

  connect(i3ipc_, &i3ipc::treeUpdated,
          this, &PlasmaI3Pager::on_window_state);

  connect(i3ipc_, &i3ipc::windowEvent,
          this, &PlasmaI3Pager::on_window_event);

  connect(i3ipc_, &i3ipc::workspacesUpdated,
          this, &PlasmaI3Pager::on_wspace_state);

  connect(i3ipc_, &i3ipc::workspaceEvent,
          this, &PlasmaI3Pager::on_wspace_event);

  connect(i3ipc_, &i3ipc::socketConnected,
          this, &PlasmaI3Pager::on_socket_connected);

  connect(i3ipc_, &i3ipc::socketDisconnected,
          this, &PlasmaI3Pager::on_socket_disconnected);

  modes_model_ = new ModesModel(this);
  qmlRegisterInterface<PlasmaI3PagerModesModel>("PlasmaI3PagerModesModel", 1);

  pager_model_ = new PagerModel(this);
  qmlRegisterInterface<PlasmaI3PagerPagerModel>("PlasmaI3PagerPagerModel", 1);

  title_model_ = new TitleModel(this);
  qmlRegisterInterface<PlasmaI3PagerTitleModel>("PlasmaI3PagerTitleModel", 1);
}

// ----------

void PlasmaI3Pager::execute() {
  LVD_LOG_T();
  LVD_SHIELD;

  bool connected = i3ipc_->connect();
  if (!connected) {
    LVD_LOG_W() << "i3ipc connect failed";
    return;
  }

  LVD_SHIELD_END;
}

// ----------

void PlasmaI3Pager::subscribe_i3mode() {
  LVD_LOG_T();

  i3ipc_->subscribe({ "mode" });
}

void PlasmaI3Pager::establish_window() {
  LVD_LOG_T();

  emit i3ipc_->fetchTree();
}

void PlasmaI3Pager::subscribe_window() {
  LVD_LOG_T();

  i3ipc_->subscribe({ "window" });
}

void PlasmaI3Pager::establish_wspace() {
  LVD_LOG_T();

  emit i3ipc_->fetchWorkspaces();
}

void PlasmaI3Pager::subscribe_wspace() {
  LVD_LOG_T();

  i3ipc_->subscribe({ "workspace" });
}

// ----------

void PlasmaI3Pager:: add_workspace (const Wspace & wspace ) {
  LVD_LOG_T() << wspace.name();

  Q_ASSERT(output_.isEmpty() || output_ == wspace.output());

  pager_model_->beginResetModel();
  LVD_FINALLY {
    pager_model_->endResetModel();
  };

  int index = 0;

  for (Wspace& wother : wspaces_) {
    if (wother.identical(wspace)) {
      LVD_LOG_D() << "identical workspace"
                  << wother.name();

      wother.integrate(wspace);
      break;
    }

    index ++;
  }

  if (index <  wspaces_.size()) {
    establish_wspace();
  }
  else {
    wspaces_.append(wspace);
  }

  sort_workspaces();
}

void PlasmaI3Pager:: del_workspace (const Wspace & wspace ) {
  LVD_LOG_T() << wspace.name();

  Q_ASSERT(output_.isEmpty() || output_ == wspace.output() || true);

  pager_model_->beginResetModel();
  LVD_FINALLY {
    pager_model_->endResetModel();
  };

  int index = 0;

  for (Wspace& wother : wspaces_) {
    if (wother.identical(wspace)) {
      LVD_LOG_D() << "identical workspace"
                  << wother.name();

      wother.integrate(wspace);
      break;
    }

    index ++;
  }

  if (index >= wspaces_.size()) {
    establish_wspace();
  }
  else {
    wspaces_.remove(index);
  }

//  sort_workspaces(); NO!
}

void PlasmaI3Pager:: upd_workspace (const Wspace & wspace ) {
  LVD_LOG_T() << wspace.name();

  Q_ASSERT(output_.isEmpty() || output_ == wspace.output());

  pager_model_->beginResetModel();
  LVD_FINALLY {
    pager_model_->endResetModel();
  };

  int index = 0;

  for (Wspace& wother : wspaces_) {
    if (wother.identical(wspace)) {
      LVD_LOG_D() << "identical workspace"
                  << wother.name();

      wother.integrate(wspace);
      break;
    }

    index ++;
  }

  if (index >= wspaces_.size()) {
    establish_wspace();
  }

  sort_workspaces();
}

void PlasmaI3Pager::oust_workspaces(const Wspaces& wspaces) {
  LVD_LOG_T();

  pager_model_->beginResetModel();
  LVD_FINALLY {
    pager_model_->endResetModel();
  };

  Wspaces wthings;

  for (const Wspace& wspace : qAsConst(wspaces)) {
    bool found = false;

    for (const Wspace& wother : qAsConst(wspaces_)) {
      if (wother.identical(wspace)) {
        LVD_LOG_D() << "identical"
                    << wother.name();

        Wspace wthing = wother;

        wthing.integrate(wspace);
        wthings.append(wthing);

        found = true;
        break;
      }
    }

    if (!found) {
      wthings.append(wspace);
    }
  }

  wspaces_ = wthings;

  sort_workspaces();
}

void PlasmaI3Pager::sort_workspaces() {
  LVD_LOG_T();

  QCollator qcollator;
  qcollator.setNumericMode(true);

  std::sort(wspaces_.begin(),
            wspaces_.end  (),
            [&] (const Wspace& lhs,
                 const Wspace& rhs) {
    return qcollator.compare(lhs.sorting_name(), rhs.sorting_name()) < 0;
  });
}

// ----------

void PlasmaI3Pager::update_output(const QString& output) {
  output_ = output;

  emit modes_model_->output_changed(output_);
  emit pager_model_->output_changed(output_);
  emit title_model_->output_changed(output_);

  establish_window();
  establish_wspace();
}

void PlasmaI3Pager::update_i3mode(const QString& i3mode) {
  i3mode_ = i3mode;
  emit modes_model_->modes_changed(i3mode);
}

void PlasmaI3Pager::update_window(const Window & window) {
  window_ = window;
  emit title_model_->title_changed(window.name());
}

// ----------

std::optional<PlasmaI3Pager::Window>
PlasmaI3Pager::search_leaf(const QJsonObject& basis) {
  LVD_LOG_T();

  auto focus_v = basis["focus"];
  if (!focus_v.isArray()) {
    LVD_LOG_W() << "invalid object";

    return std::nullopt;
  }

  auto focus_a = focus_v.toArray();
  if (focus_a.isEmpty()) {
    if (basis["type"].toString() != "workspace") {
      return PlasmaI3PagerWindow::create(basis);
    }

    return std::nullopt;
  }

  auto focus_ident_v = focus_a.first();
  if (!focus_ident_v.isDouble()) {
    LVD_LOG_W() << "invalid object";

    return std::nullopt;
  }

  auto focus_ident_l = static_cast<quint64>(focus_ident_v.toDouble());

  for (const QString& index : { "nodes", "floating_nodes" }) {
    auto nodes_v = basis[ index ];
    if (!nodes_v.isArray()) {
      LVD_LOG_W() << "invalid object";

      continue;
    }

    auto nodes_a = nodes_v.toArray();

    for (int i = 0; i < nodes_a.size(); i++) {
      auto nodex_v = nodes_a[i];
      if (!nodex_v.isObject()) {
        LVD_LOG_W() << "invalid object";

        continue;
      }

      auto nodex_o = nodex_v.toObject();

      auto nodex_ident_v = nodex_o["id"];
      if (!nodex_ident_v.isDouble()) {
        LVD_LOG_W() << "invalid object";

        continue;
      }

      auto nodex_ident_l = static_cast<quint64>(nodex_ident_v.toDouble());
      if (nodex_ident_l == focus_ident_l) {
        return search_leaf(nodex_o);
      }
    }
  }

  LVD_LOG_W() << "invalid object";

  return std::nullopt;
}

std::optional<PlasmaI3Pager::Window>
PlasmaI3Pager::search_tree(const QJsonObject& basis) {
  LVD_LOG_T();

  auto master_v = basis["nodes"];
  if (!master_v.isArray()) {
    LVD_LOG_W() << "invalid object";

    return std::nullopt;
  }

  auto master_a = master_v.toArray();

  for (int i = 0; i < master_a.size(); i++) {
    auto screen_v = master_a[i];
    if (!screen_v.isObject()) {
      LVD_LOG_W() << "invalid object";

      continue;
    }

    auto screen_o = screen_v.toObject();
    if (screen_o["name"].toString() != output_) {
      LVD_LOG_D() << "invalid object";

      continue;
    }

    auto spaces_v = screen_o["nodes"];
    if (!spaces_v.isArray()) {
      LVD_LOG_W() << "invalid object";

      continue;
    }

    auto spaces_a = spaces_v.toArray();

    for (int j = 0; j < spaces_a.size(); j++) {
      auto canvas_v = spaces_a[j];
      if (!canvas_v.isObject()) {
        LVD_LOG_W() << "invalid object";

        continue;
      }

      auto canvas_o = canvas_v.toObject();
      if (canvas_o["name"].toString() != "content") {
        LVD_LOG_D() << "invalid object";

        continue;
      }

      return search_leaf(canvas_o);
    }
  }

  LVD_LOG_W() << "invalid object";

  return std::nullopt;
}

// ----------

void PlasmaI3Pager::on_i3mode_event(const QString            i3mode,
                                    bool                     ipango) {
  Q_UNUSED(ipango)

  LVD_LOG_T() << i3mode;
  LVD_SHIELD;

  update_i3mode(i3mode);

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_window_state(const i3ipc::DataObject& window_roster) {
  LVD_LOG_T();
  LVD_SHIELD;

  if (window_roster) {
    auto window = search_tree(window_roster->first);
    if (window) {
      update_window(window.value());
    } else {
      update_window(Window());
    }
  }

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_window_event(i3ipc::WindowChange      window_change,
                                    const QJsonObject&       window_object) {
  LVD_LOG_T() << static_cast<int>(window_change);
  LVD_SHIELD;

  Window window = Window::create(window_object);

  switch (window_change) {
    case i3ipc::WindowChange::New:
      // nothing
      break;

    case i3ipc::WindowChange::Close:
      if (window.iden() == window_.iden()) {
        establish_window();
      }
      break;

    case i3ipc::WindowChange::Focus:
      if (window.output() == output_ || output_.isEmpty()) {
        update_window(window);
      }
      break;

    case i3ipc::WindowChange::Title:
      if (window.iden() == window_.iden()) {
        update_window(window);
      }
      break;

    case i3ipc::WindowChange::Fullscreen:
      // nothing
      break;

    case i3ipc::WindowChange::Move:
      establish_window();
      break;

    case i3ipc::WindowChange::Floating:
      establish_window();
      break;

    case i3ipc::WindowChange::Urgent:
      // nothing
      break;

    case i3ipc::WindowChange::Mark:
      // nothing
      break;

    case i3ipc::WindowChange::Unknown:
      establish_window();
      break;

    LVD_DEFAULT:
      LVD_LOG_W() << "unknown case"
                  << static_cast<int>(window_change)
                  <<                  window.name();
      LVD_DEFAULT_BREAK;
  }

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_wspace_state(const i3ipc::DataArray & wspace_roster) {
  LVD_LOG_T();
  LVD_SHIELD;

  if (wspace_roster) {
    QJsonArray wspace_list = wspace_roster->first;
    Wspaces    wspaces;

    for (int i = 0; i < wspace_list.size(); i ++) {
      auto wspace_data = wspace_list[i];
      if (!wspace_data.isObject()) {
        LVD_LOG_W() << "invalid object";

        continue;
      }

      auto wspace = Wspace::create(wspace_data.toObject());
      if (wspace.output() == output_ || output_.isEmpty()) {
        wspaces.append(wspace);
      }
    }

    oust_workspaces(wspaces);
  }

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_wspace_event(i3ipc::WorkspaceChange   wspace_change,
                                    const QJsonObject&       wspace_object,
                                    const QJsonObject&       wspace_former) {
  Q_UNUSED(wspace_former)

  LVD_LOG_T() << static_cast<int>(wspace_change);
  LVD_SHIELD;

  Wspace wspace = Wspace::create(wspace_object,
                                 wspace_change == i3ipc::WorkspaceChange::Urgent);

  switch (wspace_change) {
    case i3ipc::WorkspaceChange::Focus:
      {
        pager_model_->beginResetModel();
        LVD_FINALLY {
          pager_model_->endResetModel();
        };

        for (Wspace& wother : wspaces_) {
          wother.set_focused(false);

          if (wother.output() == wspace.output()) {
            wother.set_visible(false);
          }
        }
      }

      if (wspace.output() == output_) {
        wspace.set_focused(true);
        wspace.set_visible(true);

        upd_workspace(wspace);

        establish_window();
      }
      break;

    case i3ipc::WorkspaceChange::Init:
      if (wspace.output() == output_) {
        add_workspace(wspace);

        establish_window();
      }
      break;

    case i3ipc::WorkspaceChange::Empty:
      if (wspace.output() == output_) {
        del_workspace(wspace);

        establish_window();
      }
      break;

    case i3ipc::WorkspaceChange::Urgent:
      if (wspace.output() == output_) {
        upd_workspace(wspace);

        establish_window();
      }
      break;

    case i3ipc::WorkspaceChange::Reload:
      if (wspace.output() == output_) {
        upd_workspace(wspace);
      }
      break;

    case i3ipc::WorkspaceChange::Rename:
      if (wspace.output() == output_) {
        upd_workspace(wspace);
      }
      break;

    case i3ipc::WorkspaceChange::Restored:
      if (wspace.output() == output_) {
        upd_workspace(wspace);
      }
      break;

    case i3ipc::WorkspaceChange::Move:
      if (wspace.output() == output_) {
        add_workspace(wspace);
      } else {
        del_workspace(wspace);
      }
      break;

    LVD_DEFAULT:
      LVD_LOG_W() << "unknown case"
                  << static_cast<int>(wspace_change)
                  <<                  wspace.name();
      LVD_DEFAULT_BREAK;
  }

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_socket_connected() {
  LVD_LOG_T();
  LVD_SHIELD;

  subscribe_i3mode();

  establish_window();
  subscribe_window();

  establish_wspace();
  subscribe_wspace();

  LVD_SHIELD_END;
}

void PlasmaI3Pager::on_socket_disconnected() {
  LVD_LOG_T();
  LVD_SHIELD;

  QTimer::singleShot(1024, this, &PlasmaI3Pager::execute);

  LVD_SHIELD_END;
}

}  // namespace lvd::plasma::i3::pager
