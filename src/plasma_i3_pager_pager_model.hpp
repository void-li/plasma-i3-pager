/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QAbstractListModel>
#include <QByteArray>
#include <QHash>
#include <QModelIndex>
#include <QObject>
#include <QString>
#include <QVariant>

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"
#include "lvd/logger.hpp"

#include "plasma_i3_pager.hpp"

// ----------

namespace lvd::plasma::i3::pager {

class PlasmaI3PagerPagerModel : public QAbstractListModel {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaI3Pager)

  Q_PROPERTY(QString output READ output WRITE set_output NOTIFY output_changed)

 public:
  LVD_ENUM(Roles,
    DisplayName = Qt::UserRole + 1,
    SortingName,
    Focused,
    Visible,
    Urgent
  );

 public:
  PlasmaI3PagerPagerModel(PlasmaI3Pager* parent)
      : QAbstractListModel(parent),
        parent_(parent) {}

 public:
  QHash<int, QByteArray> roleNames() const override;

  int columnCount(const QModelIndex& index = QModelIndex() ) const override;
  int    rowCount(const QModelIndex& index = QModelIndex() ) const override;

  QVariant   data(const QModelIndex& index,
                  int                role = Qt::DisplayRole) const override;

 public:
  Q_INVOKABLE void select(int index);
  Q_INVOKABLE void select_l();
  Q_INVOKABLE void select_r();

 public:
  QString output() const {
    return parent_->output_;
  }
  void set_output(const QString& output) {
    parent_->update_output(output);
  }

 signals:
  void output_changed(const QString& output);

 private:
  PlasmaI3Pager* parent_ = nullptr;
  friend PlasmaI3Pager;
};

}  // namespace lvd::plasma::i3::pager
