/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>
#include <QVariantList>

#include <Plasma/Applet>  // IWYU pragma: keep
// IWYU pragma: no_include <plasma/applet.h>

#include "lvd/logger.hpp"

#include "plasma_i3_pager.hpp"

// ----------

namespace lvd::plasma::i3::pager {

class PlasmaI3PagerApplet : public Plasma::Applet {
  Q_OBJECT LVD_LOGGER

 public:
  PlasmaI3PagerApplet(QObject* parent, const QVariantList& args);

  Q_INVOKABLE PlasmaI3Pager* nativeInterface() const {
    return plasma_i3_pager_;
  }

 private:
  PlasmaI3Pager* plasma_i3_pager_ = nullptr;
};

}  // namespace lvd::plasma::i3::pager
