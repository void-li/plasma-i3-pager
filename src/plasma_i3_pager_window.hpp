/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QtGlobal>

#include <QJsonObject>
#include <QString>

// ----------

namespace lvd::plasma::i3::pager {

class PlasmaI3PagerWindow {
 public:
  static
  PlasmaI3PagerWindow create(const QJsonObject& qjsonobject);

 public:
  QString name() const { return name_; }
  quint64 iden() const { return iden_; }

  QString output() const {
    return output_;
  }

 private:
  QString name_;
  quint64 iden_;

  QString output_;
};

}  // namespace lvd::plasma::i3::pager
