/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include <QtGlobal>

#include <QJsonObject>
#include <QString>

// ----------

namespace lvd::plasma::i3::pager {

class PlasmaI3PagerWspace {
 public:
  static
  PlasmaI3PagerWspace create(const QJsonObject& qjsonobject,
                             const bool         allowurgent = true);

 public:
  bool identical(const PlasmaI3PagerWspace& plasma_i3_pager_wspace) const;
  void integrate(const PlasmaI3PagerWspace& plasma_i3_pager_wspace);

 public:
  QString         name() const { return         name_; }
  QString display_name() const { return display_name_; }
  QString sorting_name() const { return sorting_name_; }

  const std::optional<quint64>& iden() const {
    return iden_;
  }

  const std::optional<bool>& focused() const { return focused_; }
  const std::optional<bool>& visible() const { return visible_; }
  const std::optional<bool>& urgent () const { return urgent_ ; }

  void set_focused(bool focused) {
    focused_ = focused;
  }
  void set_visible(bool visible) {
    visible_ = visible;
  }

  QString output() const {
    return output_;
  }

 private:
  QString         name_;
  QString display_name_;
  QString sorting_name_;

  std::optional<quint64> iden_;

  std::optional<bool> focused_;
  std::optional<bool> visible_;
  std::optional<bool> urgent_ ;

  QString output_;
};

}  // namespace lvd::plasma::i3::pager
