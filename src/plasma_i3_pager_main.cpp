/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QMetaObject>
#include <QModelIndex>
#include <QObject>
#include <QString>
#include <QVariant>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

#include "config.hpp"
#include "plasma_i3_pager.hpp"
#include "plasma_i3_pager_modes_model.hpp"
#include "plasma_i3_pager_pager_model.hpp"
#include "plasma_i3_pager_title_model.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::plasma::i3::pager::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::plasma::i3::pager::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::plasma::i3::pager::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::plasma::i3::pager::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  try {
    lvd::plasma::i3::pager::PlasmaI3Pager plasma_i3_pager;

    QMetaObject::invokeMethod(&plasma_i3_pager, [&] {
      plasma_i3_pager.execute();
    }, Qt::QueuedConnection);

    QObject::connect(plasma_i3_pager.modes_model(),
                     &lvd::plasma::i3::pager::PlasmaI3PagerModesModel::modes_changed,
                     [&] (const QString& modes) {
      LVD_LOG_I() << "modes" << modes;
    });

    QObject::connect(plasma_i3_pager.pager_model(),
                     &lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::modelReset,
                     plasma_i3_pager.pager_model(),
                     [plasma_i3_pager_model = plasma_i3_pager.pager_model()] {
      QString message;

      for (int i = 0; i < plasma_i3_pager_model->rowCount(); i ++) {
        QModelIndex qmodelindex = plasma_i3_pager_model->index(i);

        if      (plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::Urgent ).toBool()) {
          message += "<";
          message += plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::DisplayName).toString();
          message += ">";
        }
        else if (plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::Focused).toBool()) {
          message += "{";
          message += plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::DisplayName).toString();
          message += "}";
        }
        else if (plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::Visible).toBool()) {
          message += "(";
          message += plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::DisplayName).toString();
          message += ")";
        }
        else  {
          message += "[";
          message += plasma_i3_pager_model->data(qmodelindex, lvd::plasma::i3::pager::PlasmaI3PagerPagerModel::Roles::DisplayName).toString();
          message += "]";
        }

        message += " ";
      }

      message.chop(1);

      LVD_LOG_I() << "pager" << message;
    });

    QObject::connect(plasma_i3_pager.title_model(),
                     &lvd::plasma::i3::pager::PlasmaI3PagerTitleModel::title_changed,
                     [&] (const QString& title) {
      LVD_LOG_I() << "title" << title;
    });

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
