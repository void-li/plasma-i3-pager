/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QCommandLineParser>
#include <QFile>
#include <QIODevice>
#include <QLoggingCategory>
#include <QObject>
#include <QString>

#include "logger.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class Mera01 {
 public:
  static QByteArray& __lvd_log__(Logger::Level) {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }
};

namespace mera {
class Mera02 {
 public:
  static QByteArray& __lvd_log__(Logger::Level) {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }
};
}  // namespace mera

namespace mera::luna {
class Mera03 {
 public:
  static QByteArray& __lvd_log__(Logger::Level) {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }
};
}  // namespace mera::luna

namespace {
class Mera04 {
 public:
  static QByteArray& __lvd_log__(Logger::Level) {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }
};
}  // namespace

namespace mera {
namespace {
class Mera05 {
 public:
  static QByteArray& __lvd_log__(Logger::Level) {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }
};
}  // namespace
}  // namespace mera

// ----------

QByteArray& Mera11(Logger::Level) {
  return [] (Logger::Level) -> QByteArray& {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }(Logger::Level::D);
}

namespace mera {
QByteArray Mera12(Logger::Level) {
  return [] (Logger::Level) -> QByteArray& {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }(Logger::Level::D);
}
}  // namespace mera

namespace mera::luna {
QByteArray Mera13(Logger::Level) {
  return [] (Logger::Level) -> QByteArray& {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }(Logger::Level::D);
}
}  // namespace mera::luna

namespace {
QByteArray Mera14(Logger::Level) {
  return [] (Logger::Level) -> QByteArray& {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }(Logger::Level::D);
}
}  // namespace

namespace mera {
namespace {
QByteArray Mera15(Logger::Level) {
  return [] (Logger::Level) -> QByteArray& {
    static QByteArray pretty = __PRETTY_FUNCTION__;
    return pretty;
  }(Logger::Level::D);
}
}  // namespace
}  // namespace mera

using namespace mera;
using namespace mera::luna;

// ----------

namespace like {

class Mera {
 public:
  LVD_LOGGER
};

class Luna {
 public:
  LVD_LOGGER_LIKE(Mera)
};

auto  Moon() {
  LVD_LOGFUN_LIKE(Mera)
  return &__lvd_log__;
}

}  // namespace like

// ----------

class LoggerTest : public Test {
  Q_OBJECT LVD_LOGGER

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void logger() {
    QFETCH(QByteArray, pretty);
    QFETCH(QByteArray, expect);

    QByteArray actual = Logger::logger_category(pretty);
    QCOMPARE(actual, expect);
  }

  void logger_data() {
    QTest::addColumn<QByteArray>("pretty");
    QTest::addColumn<QByteArray>("expect");

    QTest::addRow("gnucc01") << QByteArray("static QLoggingCategory& Mera01::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray(          "mera01");

    QTest::addRow("gnucc02") << QByteArray("static QLoggingCategory& mera::Mera02::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera."   "mera02");

    QTest::addRow("gnucc03") << QByteArray("static QLoggingCategory& mera::luna::Mera03::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera.luna.mera03");

    QTest::addRow("gnucc04") << QByteArray("static QLoggingCategory& {anonymous}::Mera04::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray(          "mera04");

    QTest::addRow("gnucc05") << QByteArray("static QLoggingCategory& mera::{anonymous}::Mera05::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera."   "mera05");

    QTest::addRow("clang01") << QByteArray("static QLoggingCategory &Mera01::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray(          "mera01");

    QTest::addRow("clang02") << QByteArray("static QLoggingCategory &mera::Mera02::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera."   "mera02");

    QTest::addRow("clang03") << QByteArray("static QLoggingCategory &mera::luna::Mera03::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera.luna.mera03");

    QTest::addRow("clang04") << QByteArray("static QLoggingCategory &(anonymous namespace)::Mera04::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray(          "mera04");

    QTest::addRow("clang05") << QByteArray("static QLoggingCategory &mera::(anonymous namespace)::Mera05::__lvd_log__(lvd::Logger::Level)")
                             << QByteArray("mera."   "mera05");

    QTest::addRow("native1") << Mera01::__lvd_log__(Logger::Level::D)
                             << QByteArray(          "mera01");

    QTest::addRow("native2") << Mera02::__lvd_log__(Logger::Level::D)
                             << QByteArray("mera."   "mera02");

    QTest::addRow("native3") << Mera03::__lvd_log__(Logger::Level::D)
                             << QByteArray("mera.luna.mera03");

    QTest::addRow("native4") << Mera04::__lvd_log__(Logger::Level::D)
                             << QByteArray(          "mera04");

    QTest::addRow("native5") << Mera05::__lvd_log__(Logger::Level::D)
                             << QByteArray("mera."   "mera05");
  }

  void logger_like() {
    QCOMPARE(  like::Luna::__lvd_log__(Logger::Level::D).categoryName(),
             "like.mera");
  }

  // ----------

  void logfun() {
    QFETCH(QByteArray, pretty);
    QFETCH(QByteArray, expect);

    QByteArray actual = Logger::logfun_category(pretty);
    QCOMPARE(actual, expect);
  }

  void logfun_data() {
    QTest::addColumn<QByteArray>("pretty");
    QTest::addColumn<QByteArray>("expect");

    QTest::addRow("gnucc11") << QByteArray("Mera11(lvd::Logger::Level)::<lambda(lvd::Logger::Level)>")
                             << QByteArray(          "mera11");

    QTest::addRow("gnucc12") << QByteArray("mera::Mera12(lvd::Logger::Level)::<lambda(lvd::Logger::Level)>")
                             << QByteArray("mera."   "mera12");

    QTest::addRow("gnucc13") << QByteArray("mera::luna::Mera13(lvd::Logger::Level)::<lambda(lvd::Logger::Level)>")
                             << QByteArray("mera.luna.mera13");

    QTest::addRow("gnucc14") << QByteArray("{anonymous}::Mera14(lvd::Logger::Level)::<lambda(lvd::Logger::Level)>")
                             << QByteArray(          "mera14");

    QTest::addRow("gnucc15") << QByteArray("mera::{anonymous}::Mera15(lvd::Logger::Level)::<lambda(lvd::Logger::Level)>")
                             << QByteArray("mera."   "mera15");

    QTest::addRow("clang11") << QByteArray("auto Mera11(lvd::Logger::Level)::(anonymous class)::operator()(lvd::Logger::Level) const")
                             << QByteArray(          "mera11");

    QTest::addRow("clang12") << QByteArray("auto mera::Mera12(lvd::Logger::Level)::(anonymous class)::operator()(lvd::Logger::Level) const")
                             << QByteArray("mera."   "mera12");

    QTest::addRow("clang13") << QByteArray("auto mera::luna::Mera13(lvd::Logger::Level)::(anonymous class)::operator()(lvd::Logger::Level) const")
                             << QByteArray("mera.luna.mera13");

    QTest::addRow("clang14") << QByteArray("auto (anonymous namespace)::Mera14(lvd::Logger::Level)::(anonymous class)::operator()(lvd::Logger::Level) const")
                             << QByteArray(          "mera14");

    QTest::addRow("clang15") << QByteArray("auto mera::(anonymous namespace)::Mera15(lvd::Logger::Level)::(anonymous class)::operator()(lvd::Logger::Level) const")
                             << QByteArray("mera."   "mera15");

    QTest::addRow("native1") << Mera11(Logger::Level::D)
                             << QByteArray(          "mera11");

    QTest::addRow("native2") << Mera12(Logger::Level::D)
                             << QByteArray("mera."   "mera12");

    QTest::addRow("native3") << Mera13(Logger::Level::D)
                             << QByteArray("mera.luna.mera13");

    QTest::addRow("native4") << Mera14(Logger::Level::D)
                             << QByteArray(          "mera14");

    QTest::addRow("native5") << Mera15(Logger::Level::D)
                             << QByteArray("mera."   "mera15");
  }

  void logfun_like() {
    QCOMPARE((*like::Moon())          (Logger::Level::D).categoryName(),
             "like.mera");
  }

  // ----------

  void loghull() {
    LVD_LOGBUF message;
    LVD_LOG_C(&message) << LVD_LOGHULL("Mera Luna A");

    QCOMPARE(message,   "Mera Luna A");
  }

  void loghusk() {
    LVD_LOGBUF message;
    LVD_LOG_C(&message) << LVD_LOGHUSK(QString message = "Mera Luna A";
                                       return  message);

    QCOMPARE(message, "\"Mera Luna A\"");
  }

  void loghull_executed() {
    Logger::add_logrule("* = true ");
    Logger::refresh();

    QString qstring = "Mera";
    LVD_LOG_C() << LVD_LOGHULL(((qstring = "Luna") == "Luna") ? "Mera Luna A"
                                                              : "Mera Luna A");

    QCOMPARE(qstring, "Luna");
  }

  void loghull_pristine() {
    Logger::add_logrule("* = false");
    Logger::refresh();

    QString qstring = "Mera";
    LVD_LOG_C() << LVD_LOGHULL(((qstring = "Luna") == "Luna") ? "Mera Luna A"
                                                              : "Mera Luna A");

    QCOMPARE(qstring, "Mera");
  }

  void logline() {
    QString qstring = "Mera Luna A\nMera Luna B";

    LVD_LOGBUF message;
    LVD_LOG_C(&message) << LVD_LOGLINE(qstring);

    QCOMPARE(message, "\"Mera Luna A\" ...");
  }

  // ----------

  void LogLevel_C() {
    LVD_LOGBUF message;
    LVD_LOG_C(&message) << "Mera Luna C";

    QCOMPARE(message,            "Mera Luna C");
  }

  void LogLevel_W() {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "Mera Luna W";

    QCOMPARE(message,            "Mera Luna W");
  }

  void LogLevel_I() {
    LVD_LOGBUF message;
    LVD_LOG_I(&message) << "Mera Luna I";

    QCOMPARE(message,            "Mera Luna I");
  }

  void LogLevel_D() {
    LVD_LOGBUF message;
    LVD_LOG_D(&message) << "Mera Luna D";

    QCOMPARE(message,            "Mera Luna D");
  }

  void LogLevel_T() {
    LVD_LOGBUF message;
    LVD_LOG_T(&message) << "Mera Luna T";

    QCOMPARE(message, "LogLevel_T Mera Luna T");
  }

  // ----------

  void install_and_restore() {
    Logger::install();
    LogLevel_C();
    LogLevel_W();
    LogLevel_I();
    LogLevel_D();
    LogLevel_T();
    Logger::restore();
  }

  // ----------

  void logconf() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    QFile qfilea("logger_test.a.conf");
    qfilea.open(QIODevice::WriteOnly);
    QVERIFY(qfilea.isOpen());

    qfilea.write("[lvd_log]"                  "\n");
    qfilea.write("logger_test.a "  " = false" "\n");
    qfilea.close();

    Logger::add_logconf("logger_test.a.conf");
    Logger::refresh();

    if (!none_enabled(qloggingcategorya)) {
      return;
    }
  }

  void logconf2() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    QFile qfilea("logger_test.a.conf");
    qfilea.open(QIODevice::WriteOnly);
    QVERIFY(qfilea.isOpen());

    qfilea.write("[lvd_log]"                  "\n");
    qfilea.write("logger_test.a "  " = false" "\n");
    qfilea.write("logger_test.a.info = true " "\n");
    qfilea.close();

    Logger::add_logconf("logger_test.a.conf");
    Logger::refresh();

    if (!info_enabled(qloggingcategorya)) {
      return;
    }
  }

  void logconfs() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    QLoggingCategory qloggingcategoryb("logger_test.b");
    if (!full_enabled(qloggingcategoryb)) return;

    QFile qfilea("logger_test.a.conf");
    qfilea.open(QIODevice::WriteOnly);
    QVERIFY(qfilea.isOpen());

    qfilea.write("[lvd_log]"                  "\n");
    qfilea.write("logger_test.a "  " = false" "\n");
    qfilea.close();

    Logger::add_logconf("logger_test.a.conf");
    Logger::refresh();

    QFile qfileb("logger_test.b.conf");
    qfileb.open(QIODevice::WriteOnly);
    QVERIFY(qfileb.isOpen());

    qfileb.write("[lvd_log]"                  "\n");
    qfileb.write("logger_test.b "  " = false" "\n");
    qfileb.close();

    Logger::add_logconf("logger_test.b.conf");
    Logger::refresh();

    if (!none_enabled(qloggingcategorya)) {
      return;
    }

    if (!none_enabled(qloggingcategoryb)) {
      return;
    }
  }

  void logrule() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    Logger::add_logrule("logger_test.a "  " = false");
    Logger::refresh();

    if (!none_enabled(qloggingcategorya)) {
      return;
    }
  }

  void logrule2() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    Logger::add_logrule("logger_test.a "  " = false");
    Logger::refresh();

    Logger::add_logrule("logger_test.a.info = true ");
    Logger::refresh();

    if (!info_enabled(qloggingcategorya)) {
      return;
    }
  }

  void logrule3() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    Logger::add_logrule("logger_test.a "  " = false");
    Logger::refresh();

    QLoggingCategory qloggingcategoryb("logger_test.a.trace");

    if (!none_enabled(qloggingcategoryb)) {
      return;
    }
  }

  void logrules() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    QLoggingCategory qloggingcategoryb("logger_test.b");
    if (!full_enabled(qloggingcategoryb)) return;

    Logger::add_logrule("logger_test.a "  " = false");
    Logger::add_logrule("logger_test.b "  " = false");
    Logger::refresh();

    if (!none_enabled(qloggingcategorya)) {
      return;
    }

    if (!none_enabled(qloggingcategoryb)) {
      return;
    }
  }

  void logrules2() {
    QLoggingCategory qloggingcategorya("logger_test.a");
    if (!full_enabled(qloggingcategorya)) return;

    QLoggingCategory qloggingcategoryb("logger_test.b");
    if (!full_enabled(qloggingcategoryb)) return;

    Logger::add_logrule("logger_test.a "  " = false"
                    ";" "logger_test.b "  " = false");
    Logger::refresh();

    if (!none_enabled(qloggingcategorya)) {
      return;
    }

    if (!none_enabled(qloggingcategoryb)) {
      return;
    }
  }

  void log2sys() {
    QCommandLineParser qcommandlineparser;
    Logger::setup_arguments(qcommandlineparser);

    qcommandlineparser.parse({ "logger_test", "--log2sys" });
    Logger::parse_arguments(qcommandlineparser);

    Logger::install();

    LVD_FINALLY {
      Logger::restore();
    };

    LVD_LOGBUF message;
    LVD_LOG_C(&message) << "Mera Luna C";
    LVD_LOG_W(&message) << "Mera Luna W";
    LVD_LOG_I(&message) << "Mera Luna I";
    LVD_LOG_D(&message) << "Mera Luna D";
    LVD_LOG_T(&message) << "Mera Luna T";
  }

 private:
  bool full_enabled(const QLoggingCategory& qloggingcategory) {
    bool success = false;

    [&] {
      QVERIFY( qloggingcategory.   isDebugEnabled());
      QVERIFY( qloggingcategory.    isInfoEnabled());
      QVERIFY( qloggingcategory. isWarningEnabled());
      QVERIFY( qloggingcategory.isCriticalEnabled());

      success = true;
    }();

    return success;
  }

  bool none_enabled(const QLoggingCategory& qloggingcategory) {
    bool success = false;

    [&] {
      QVERIFY(!qloggingcategory.   isDebugEnabled());
      QVERIFY(!qloggingcategory.    isInfoEnabled());
      QVERIFY(!qloggingcategory. isWarningEnabled());
      QVERIFY(!qloggingcategory.isCriticalEnabled());

      success = true;
    }();

    return success;
  }

  bool info_enabled(const QLoggingCategory& qloggingcategory) {
    bool success = false;

    [&] {
      QVERIFY(!qloggingcategory.   isDebugEnabled());
      QVERIFY( qloggingcategory.    isInfoEnabled());
      QVERIFY(!qloggingcategory. isWarningEnabled());
      QVERIFY(!qloggingcategory.isCriticalEnabled());

      success = true;
    }();

    return success;
  }

  // ----------

 private slots:
  void init() {
    Test::init();

    Logger::clear_logconfs();
    Logger::clear_logrules();
    Logger::refresh();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };

    Logger::clear_logconfs();
    Logger::clear_logrules();
    Logger::refresh();
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(LoggerTest)
#include "logger_test.moc"  // IWYU pragma: keep
