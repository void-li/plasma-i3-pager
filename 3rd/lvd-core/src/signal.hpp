/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <signal.h>  // IWYU pragma: export

#include <QObject>
#include <QString>

// ----------

namespace lvd {

class Signal : public QObject {
  Q_OBJECT

 public:
  Signal(int signal, QObject* parent = nullptr);

 signals:
  void activated();
};

}  // namespace lvd
