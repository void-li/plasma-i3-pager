/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once
#include "core.hpp"

#include <stack>

// ----------

namespace lvd {

template <class T>
class Task {
 protected:
  using State  =            bool (T::*)(void);
  using States = std::stack<bool (T::*)(void)>;

 protected:
  void exec_state();
  auto head_state() -> State;

  void push_state(State state);
  auto pull_state() -> State;

  template <typename... States>
  bool drop_state (States... states);

  template <typename... States>
  bool drop_states(States... states);

  auto size_states() const -> size_t;

  bool empty_states() const;
  void clear_states();

 private:
  States states_;
};

}  // namespace lvd
